export const state = () => ({
  show_order: false,
  is_persist: false,
  order: [],
  // base: 'http://localhost/tmmnew/api_cctv/',
  //base: 'http://localhost/w_back_end/'
  base: 'https://tokomanamana.com/api_cctv/'
})

export const mutations = {
  push_order (state, data) {
    const formatter = new Intl.NumberFormat('en-US', {
      style: 'currency',
      currency: 'IDR',
      minimumFractionDigits: 0
    })
    let d = []
    for (let a=0; a<data.length; a++) {
      d.push(data[a])
      d[a].harga = formatter.format(d[a].harga)
    }
    state.order = d
  },
  change_show_order (state, data) {
    if (data === 0) {
      state.show_order = false
    } else if (data === 1) {
      state.show_order = true
      state.is_persist = true
    } else if (data === 2) {
      state.show_order = true
      state.is_persist = false
    }
  },
  add (state, text) {
    state.list.push({
      text: text,
      done: false
    })
  },
  remove (state, { todo }) {
    state.list.splice(state.list.indexOf(todo), 1)
  },
  toggle (state, todo) {
    todo.done = !todo.done
  }
}