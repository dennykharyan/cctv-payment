import colors from 'vuetify/es5/util/colors'

export default {
	mode: 'spa',

	/* Use This Setting For Testing Via Mobile Device */
		// server: {
		//   port: 8000,
		//   host: '0.0.0.0'
		// },
	/* */
  
	/*
	** Headers of the page
	*/
  	head: {
		// titleTemplate: '%s - ' + process.env.npm_package_name,
		titleTemplate: 'Payment - Tokomanamana.com',
		title: 'Payment - Tokomanamana.com',
		meta: [
			{ charset: 'utf-8' },
			{ name: 'viewport', content: 'width=device-width, initial-scale=1' },
			{
				hid: 'description',
				name: 'description',
				content: process.env.npm_package_description || ''
			}
		],
		link: [
			// { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
			{ rel: 'shortcut icon', type: 'image/x-icon', href: 'https://tokomanamana.com/assets/frontend/images/tokomanamana.ico' },
			{
				rel: 'stylesheet',
				href:
				'https://fonts.googleapis.com/css?family=Roboto:500&display=swap|Material+Icons'
			}
		]
	},
	/*
		** Customize the progress-bar color
	*/
		// loading: { color: '#F29D4D' },
		loading: '~/components/loading.vue',
	/*
		** Global CSS
	*/
	css: ['~/assets/main.css'],
	/*
		** Plugins to load before mounting the App
	*/
	plugins: [
		{ src: '~/plugins/plugin', ssr: false }, // error alert plugin for the app
		{ src: '~/plugins/i18n.js' }
	],
	/*
		** Nuxt.js modules
	*/
	modules: [
		'@nuxtjs/vuetify',
		// Doc: https://axios.nuxtjs.org/usage
		'@nuxtjs/axios',
		'@nuxtjs/pwa',
		// '@nuxtjs/eslint-module'
		'@nuxtjs/proxy',
		'nuxt-clipboard2'
	],
	/*
		** Axios module configuration
		** See https://axios.nuxtjs.org/options
	*/
	proxy: {
		'/api/': {
			target: 'https://tokomanamana.com/api_cctv',
			// target: 'http://localhost/tmm/api_cctv',
			// target: 'http://localhost/w_back_end',
			ws: false,
			changeOrigin: true,
			proxyHeaders: true,
			credentials: true,
			pathRewrite: {
				'^/api/' : ''
			}
		},
		'/v_api/': {
			target: 'https://tokomanamana.com/api_cctv',
			// target: 'http://localhost/tmm/api_cctv',
			// target: 'http://localhost/w_back_end',
			ws: false,
			changeOrigin: true,
			credentials: true,
			pathRewrite: {
				'^/v_api/' : ''
			}
		}
	},
	axios: {
		proxy: true
	},
	router: {
		base: '/payment'
	},
	/*
		** vuetify module configuration
		** https://github.com/nuxt-community/vuetify-module
	*/
	vuetify: {
		theme: {
			primary: '#F29D4D',
			accent: colors.grey.darken3,
			// secondary: colors.amber.darken3,
			secondary: '#a7c22a',
			info: colors.teal.lighten1,
			warning: colors.amber.base,
			error: colors.deepOrange.accent4,
			success: colors.green.accent3
		}
	},
	/*
		** Build configuration
	*/
	build: {
		/*
			** You can extend webpack config here
		*/
		extend(config, ctx) {}
	}
}
