import Vue from 'vue'
import VueI18n from 'vue-i18n'

Vue.use(VueI18n)

export default ({ app }) => {
    app.i18n = new VueI18n({
      locale: 'id',
      fallbackLocale: 'id',
      messages: {
        en: require('~/locales/en.json'),
        id: require('~/locales/id.json'),
        it: require('~/locales/it.json'),
        pt: require('~/locales/pt.json'),
        ru: require('~/locales/ru.json'),
        zh: require('~/locales/zh.json'),
      }
    })
  }