export default {
    home: {
        title: 'Detail',
        rent_time: 'Waktu Transaksi',
        recording_method: 'Metode Perekaman',
        choose_camera: 'Pilih Kamera',
        choose_duration: 'Pilih Durasi Penyimpanan',
        detail: 'Detail',
        time: 'Waktu',
        camera_choice: 'Kamera Pilihan',
        camera_quantity: 'Jumlah Kamera',
        duration: 'Durasi',
        total: 'Total',
        submit: 'Kirim'
    }
}