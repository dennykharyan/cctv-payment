export default {
    title: 'Detail',
    rent_time: 'Rent Time',
    recording_method: 'Recording Method',
    choose_camera: 'Choose Camera',
    choose_duration: 'Choose Duration',
    detail: 'Detail',
    time: 'Time',
    camera_choice: 'Camera Choice',
    camera_quantity: 'Camera Quantity',
    duration: 'Duration',
    total: 'Total',
    submit: 'Submit',
    home: {
        
    }
}