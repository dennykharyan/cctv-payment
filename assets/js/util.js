const util = {
    rupiah (x) {
        let val = (x/1).toFixed(2).replace('.', ',')
        return 'Rp. ' + val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
    }
}

export default util